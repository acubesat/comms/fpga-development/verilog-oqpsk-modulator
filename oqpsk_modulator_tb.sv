`timescale 1ns / 1ps
`include "oqpsk_helper3.sv" 
module OQPSK_MODULATOR_v1_0_tb;

// Parameters of the OQPSK_MODULATOR_v1_0 instance
localparam C_S00_AXIS_TDATA_WIDTH = 64;
localparam C_M00_AXIS_TDATA_WIDTH = 16;
localparam C_M01_AXIS_TDATA_WIDTH = 16;

// Testbench signals
reg aclk;
reg aresetn;


reg s00_axis_tvalid;
reg [C_S00_AXIS_TDATA_WIDTH-1:0] s00_axis_tdata;
reg [C_S00_AXIS_TDATA_WIDTH/8-1:0] s00_axis_tkeep;
reg s00_axis_tlast;
wire s00_axis_tready;


wire m00_axis_tvalid;
wire [C_M00_AXIS_TDATA_WIDTH-1:0] m00_axis_tdata;
reg m00_axis_tready;
wire m00_axis_tlast;


wire m01_axis_tvalid;
wire [C_M01_AXIS_TDATA_WIDTH-1:0] m01_axis_tdata;
reg m01_axis_tready;
wire m01_axis_tlast;

// Instance of the OQPSK_MODULATOR_v1_0
OQPSK_MODULATOR_v1_0 #(
    .C_S00_AXIS_TDATA_WIDTH(C_S00_AXIS_TDATA_WIDTH),
    .C_M00_AXIS_TDATA_WIDTH(C_M00_AXIS_TDATA_WIDTH),
    .C_M01_AXIS_TDATA_WIDTH(C_M01_AXIS_TDATA_WIDTH)
) modulator_inst (
    .aclk(aclk),
    .aresetn(aresetn),
    .s00_axis_tvalid(s00_axis_tvalid),
    .s00_axis_tdata(s00_axis_tdata),
    .s00_axis_tready(s00_axis_tready),
    .s00_axis_tkeep(s00_axis_tkeep),
    .s00_axis_tlast(s00_axis_tlast),
    .m00_axis_tvalid(m00_axis_tvalid),
    .m00_axis_tdata(m00_axis_tdata),
    .m00_axis_tready(m00_axis_tready),
    .m00_axis_tlast(m00_axis_tlast),
    .m01_axis_tvalid(m01_axis_tvalid),
    .m01_axis_tdata(m01_axis_tdata),
    .m01_axis_tready(m01_axis_tready),
    .m01_axis_tlast(m01_axis_tlast)
);

// Clock generation
initial begin
    aclk = 0;
    forever #5 aclk = ~aclk; // 100MHz clock
end

// Stimulus here
initial begin
    $dumpfile("oqpsk_results.vcd");
    // Dump all signals in the testbench and UUT
    $dumpvars(0, OQPSK_MODULATOR_v1_0_tb);
    // Initialize inputs
    m01_axis_tready = 0;
    m00_axis_tready = 0;
    #5;
    aresetn = 0;
    #10;
    s00_axis_tvalid = 1;
    aresetn = 1;
    #100;
    s00_axis_tdata = 64'b10100101101001011010010110100101101001011010_0101_1010_0101_1010_0101;    
    m01_axis_tready = 1;
    m00_axis_tready = 1;
    s00_axis_tkeep = -1;
    s00_axis_tlast = 0;
    #10;
    s00_axis_tvalid = 0;
    #1400;
    s00_axis_tvalid = 1;
    s00_axis_tdata = 64'b10100101101001011010010110100101101001011010_0101_1010_0101_1010_0000;
    #10;
    s00_axis_tvalid = 0;
    // Wait for reset deassertion
    // wait(aresetn == 1);
    #200;
    
    // // Example stimulus
    // s00_axis_tvalid = 1;
    // s00_axis_tdata = {C_S00_AXIS_TDATA_WIDTH{1'b1}}; // Example data
    // s00_axis_tkeep = {C_S00_AXIS_TDATA_WIDTH/8{1'b1}};
    // s00_axis_tlast = 1;
    #100;
    
    s00_axis_tvalid = 0;
    s00_axis_tlast = 0;
    $finish;
    // Further stimulus as required
end

endmodule